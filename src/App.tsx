import React, { useState } from "react";
import { useQuery } from "react-query";
import {
  Container,
  Spinner,
  Navbar,
  Card,
  Button,
  Badge,
  Offcanvas,
} from "react-bootstrap";
import Cart from "./Cart/Cart";

import "bootstrap/dist/css/bootstrap.min.css";

const getProducts = async (): Promise<CartItemType[]> =>
  await (await fetch("https://fakestoreapi.com/products")).json();

function App() {
  const [cartOpen, setCartOpen] = useState(false);
  const [cartItems, setCartItems] = useState([] as CartItemType[]);
  const { data, isLoading, error } = useQuery<CartItemType[]>(
    "products",
    getProducts
  );

  const getTotalItems = (items: CartItemType[]) =>
    items.reduce((ack: number, item) => ack + item.amount, 0);

  const handleAddToCart = (clickedItem: CartItemType) => {
    setCartItems((prev) => {
      // 1. Is the item already added in the cart?
      const isItemInCart = prev.find((item) => item.id === clickedItem.id);

      if (isItemInCart) {
        return prev.map((item) =>
          item.id === clickedItem.id
            ? { ...item, amount: item.amount + 1 }
            : item
        );
      }
      // First time the item is added
      return [...prev, { ...clickedItem, amount: 1 }];
    });
  };

  const handleRemoveFromCart = (id: number) => {
    setCartItems((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (item.amount === 1) return ack;
          return [...ack, { ...item, amount: item.amount - 1 }];
        } else {
          return [...ack, item];
        }
      }, [] as CartItemType[])
    );
  };

  if (isLoading)
    return (
      <Spinner
        animation="border"
        role="status"
        className="m-auto fixed-top fixed-left fixed-right fixed-bottom"
      >
        <span className="visually-hidden">Loading...</span>
      </Spinner>
    );
  if (error) return <div>Something went wrong ...</div>;
  return (
    <div>
      <Navbar bg="light" fixed="top">
        <Container>
          <Navbar.Brand>Shopping Cart</Navbar.Brand>
          <Button variant="primary" onClick={() => setCartOpen(true)}>
            Cart <Badge bg="secondary">{getTotalItems(cartItems)}</Badge>
            <span className="visually-hidden">unread messages</span>
          </Button>
        </Container>
      </Navbar>
      <div className="d-flex flex-wrap justify-content-center">
        {data?.map((item) => (
          <Card key={item.id} className="m-2 text-center" style={{ maxWidth: "20%" }}>
            <Card.Img variant="top" src={item.image} height={400} />
            <Card.Body>
              <Button className="m-3" variant="primary" onClick={() => handleAddToCart(item)}>
                Add to cart
              </Button>
              <Card.Title>{item.title}</Card.Title>
              <Card.Text style={{textAlign: 'left'}}>{item.description}</Card.Text>
            </Card.Body>
          </Card>
        ))}
      </div>
      <Offcanvas
        show={cartOpen}
        placement="end"
        onHide={() => setCartOpen(false)}
      >
        <Offcanvas.Header closeButton>
          <Offcanvas.Title></Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Cart
            cartItems={cartItems}
            addToCart={handleAddToCart}
            removeFromCart={handleRemoveFromCart}
          />
        </Offcanvas.Body>
      </Offcanvas>
    </div>
  );
}

export default App;
