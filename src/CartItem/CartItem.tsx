import { Button } from "react-bootstrap";
// Types
type Props = {
  item: CartItemType;
  addToCart: (clickedItem: CartItemType) => void;
  removeFromCart: (id: number) => void;
};

const CartItem: React.FC<Props> = ({ item, addToCart, removeFromCart }) => (
    <div className="border-top border-dark m-3 p-2">
      <h3>{item.title}</h3>
      <div className="">
        <p>Price: ${item.price}</p>
        <p>Total: ${(item.amount * item.price).toFixed(2)}</p>
      </div>
      <img src={item.image} alt={item.title} height={200} width={200} />
      <div className="d-inline-flex w-50 justify-content-between m-2">
        <Button  onClick={() => removeFromCart(item.id)}>-</Button>
        <p>{item.amount}</p>
        <Button  onClick={() => addToCart(item)}>+</Button>
      </div>
    </div> 
);

export default CartItem;
